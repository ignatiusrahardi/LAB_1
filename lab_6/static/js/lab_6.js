var print = document.getElementById('print');
var erase = false;
var arr = [
    {"id":0,"text":"Red","bcgColor":"#F44336","fontColor":"#FAFAFA"},
    {"id":1,"text":"Pink","bcgColor":"#E91E63","fontColor":"#FAFAFA"},
    {"id":2,"text":"Purple","bcgColor":"#9C27B0","fontColor":"#FAFAFA"},
    {"id":3,"text":"Indigo","bcgColor":"#3F51B5","fontColor":"#FAFAFA"},
    {"id":4,"text":"Blue","bcgColor":"#2196F3","fontColor":"#212121"},
    {"id":5,"text":"Teal","bcgColor":"#009688","fontColor":"#212121"},
    {"id":6,"text":"Lime","bcgColor":"#CDDC39","fontColor":"#212121"},
    {"id":7,"text":"Yellow","bcgColor":"#FFEB3B","fontColor":"#212121"},
    {"id":8,"text":"Amber","bcgColor":"#FFC107","fontColor":"#212121"},
    {"id":9,"text":"Orange","bcgColor":"#FF5722","fontColor":"#212121"},
    {"id":10,"text":"Brown","bcgColor":"#795548","fontColor":"#FAFAFA"}
];

var themes = JSON.stringify(arr);

var go = function(x) {
  if (x === 'ac') {
  	print    value = "";
    /* implemetnasi clear all */
  } else if (x === 'eval') {
      print.value = Math.round(evil(print.value) * 10000) / 10000;
      erase = true;
  } else if (x ==='sin') {
      print.value = Math.sin(print.value);
      erase = true;
  } else if (x ==='tan') {
      print.value = Math.tan(print.value);
      erase = true;
  } else if (x ==='log') {
      print.value = Math.log(print.value);
      erase = true;
  } else {
    print.value += x;
  }
};

function evil(fn) {
  return new Function('return ' + fn)();
}

var anon = [

  "halo",
  "ola",
  "kau tidak tahu siapa aku hah?!",
  "terima kasih banyak oyyyy",
  "kamu disini dan kau disana",
  "i am a human coyyyyy",
  "LETS SE GOOOO",
  "HOLA"

]
//CHAT

$(document).ready(function(){
	$("#chat").keypress(function(e){
	var key = e.which;
 	if(key == 13)  
  	{
  		var item = anon[Math.floor(Math.random()*anon.length)];
  		$("#insert").append("<div class=\"msg-send\">"  + "me: " + $("textarea").val() + "</div>");
      setTimeout(function () {
                     $("#insert").append("<div class=\"msg-receive\">"  + "anon: " + item + "</div>");
                 }, 3000);
      
  		$("textarea").val('');
  		event.preventDefault();
  	}
	
	});

});
//END

function changeTheme(theme) {
  $('body').css('background-color',theme.bcgColor);
  $('body').css('color',theme.fontColor);
}
//local storage = menyimpan var di browser
localStorage.setItem('added-items', themes);


$(document).ready(function() { //documen ready : suatu func dmn dia akan menjalankan semua func didalamnya jika doc udh di load
    $('.my-select').select2({
    'data': JSON.parse(themes)
});
});


var themeNow = {"Indigo": {"bcgColor":"#3F51B5","fontColor":"#FAFAFA"}};
if (!localStorage.getItem("selectedTheme")) {
  localStorage.setItem("selectedTheme", JSON.stringify(themeNow['Indigo']))
}
changeTheme(JSON.parse(localStorage.getItem("selectedTheme")))

$('.apply-button').on('click', function() {  // sesuaikan class button
    // [TODO] ambil value dari elemen select .my-select
    var id = $('.my-select').val() 

    // [TODO] cocokan ID theme yang dipilih dengan daftar theme yang ada
    // [TODO] ambil object theme yang dipilih
    var objTheme = JSON.parse(localStorage.getItem('added-items'))[id];
    // [TODO] aplikasikan perubahan ke seluruh elemen HTML yang perlu diubah warnanya
    changeTheme(objTheme);
    // [TODO] simpan object theme tadi ke local storage selectedTheme
    localStorage.setItem("selectedTheme", JSON.stringify(objTheme));
});



