from django.test import TestCase
from .views import (
	index, add_friend, validate_npm, delete_friend, friend_list,
	get_friend_list, paginate_page
)
from unittest.mock import patch
from django.db.models.manager import Manager
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .models import Friend
from .api_csui_helper.csui_helper import CSUIhelper
# Create your tests here.

csui_helper = CSUIhelper()

class Lab7UnitTest(TestCase):

	def test_lab_7_url_is_exist(self):
		response = Client().get('/lab-7/')
		self.assertEqual(response.status_code, 200)

	def test_lab7_using_index_func(self):
		found = resolve('/lab-7/')
		self.assertEqual(found.func, index)
		

	def test_lab7_daftar_teman_url_is_exist(self):
		response = Client().get('/lab-7/friends-list/')
		self.assertEqual(response.status_code, 200)

	def test_lab7_get_friends_list_data_url_is_exist(self):
		response = Client().get('/lab-7/get-friend-list/')
		self.assertEqual(response.status_code, 200)


	def test_lab7_add_friend_success(self):
		nama = 'Ignatius R'
		npm = '1606875951'
		data = {'name': nama, 'npm': npm}
		response_post = Client().post('/lab-7/add-friend/', data)
		self.assertEqual(response_post.status_code, 200)

	def test_delete_friend(self):
		friend = Friend.objects.create(friend_name="Patricia", npm="1606874633")
		response = Client().post('/lab-7/friends-list/delete-friend/' + str(friend.id) + '/')
		self.assertEqual(response.status_code, 302)
		self.assertNotIn(friend, Friend.objects.all())

	def test_validate_npm(self):
		response = self.client.post('/lab-7/validate-npm/')
		html_response = response.content.decode('utf8')
		self.assertEqual(response.status_code, 200)
		self.assertJSONEqual(html_response, {'is_taken': False})

	def test_invalid_page_pagination_number(self):
		data = ["asik", "hehe", "seru", "asik", "hehe", "seru", "asik", "hehe", "seru", "asik",
				"hehe", "seru", "asik", "hehe", "seru", "asik", "hehe", "seru", "asik", "hehe",
				"seru", "asik", "hehe", "seru", "asik", "hehe", "seru", "asik", "hehe", "seru"]
		test1 = paginate_page("...", data)
		test2 = paginate_page(-1, data)
		with patch.object(Manager, 'get_or_create') as a:
			a.side_effect = PageNotAnInteger("page number is not an integer")
			res = paginate_page("...", data)
			self.assertEqual(res['page_range'], test1['page_range'])
		with patch.object(Manager, 'get_or_create') as a:
			a.side_effect = EmptyPage("page number is less than 1")
			res = paginate_page(-1, data)
			self.assertEqual(res['page_range'], test2['page_range'])

	def test_lab_7_csui_helper_initiate(self):
		
		self.assertTrue(csui_helper.instance is not None)

	def test_lab_7_csui_helper_get_mahasiswa_list(self):
		
		mahasiswa_list, prev_exists, next_exists = csui_helper.instance.get_mahasiswa_list(page=5)
		self.assertTrue(mahasiswa_list is not None and prev_exists and next_exists)

	def test_lab_7_get_access_token(self):
		
		self.assertTrue(csui_helper.instance.get_access_token() is not None)

	def test_lab_7_get_client_id(self):
		
		self.assertTrue(csui_helper.instance.get_client_id() is not None)

	def test_lab_7_get_auth_param_dict(self):
		
		dict = csui_helper.instance.get_auth_param_dict()
		self.assertTrue('access_token' in dict and 'client_id' in dict)

	def test_lab_7_invalid_credentials(self):
		csui_helper.instance.test_change_credentials()
		with self.assertRaises(Exception):
			csui_helper.instance.get_access_token()
		
	def test_empty_page(self):
		response = Client().get('/lab-7/?page=-9')
		self.assertRaises(EmptyPage)

	
	def test_invalid_page(self):
		response = Client().get('/lab-7/?page=*')
		self.assertRaises(PageNotAnInteger)

