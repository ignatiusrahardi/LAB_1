 // FB initiation function
  window.fbAsyncInit = () => {
    FB.init({
      appId      : '749398025252524',
      cookie     : true,
      xfbml      : true,
      version    : 'v2.11'
    });

   $('body').append('<div id="fb-root"></div>');
    // implementasilah sebuah fungsi yang melakukan cek status login (getLoginStatus)
    // dan jalankanlah fungsi render dibawah, dengan parameter true jika
    // status login terkoneksi (connected)

    // Hal ini dilakukan agar ketika web dibuka, dan ternyata sudah login, maka secara
    // otomatis akan ditampilkan view sudah login
    FB.getLoginStatus(function(response) {
      if (response.status === 'connected') {
        console.log('connected');
        render(true);
      }
      else {
        console.log('not connected');
      }
  });

  };

  // Call init facebook. default dari facebook
  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "https://connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));

  // Fungsi Render, menerima parameter loginFlag yang menentukan apakah harus
  // merender atau membuat tampilan html untuk yang sudah login atau belum
  // Rubah metode ini seperlunya jika kalian perlu mengganti tampilan dengan memberi
  // Class-Class Bootstrap atau CSS yang anda implementasi sendiri
  const render = loginFlag => {
    console.log('render')
    if (loginFlag) {
      console.log('Welcome!  Fetching your information.... ');
      FB.api('/me', function(response){
        console.log('Good to see you, ' + response.name + '.');
      });
      // Jika yang akan dirender adalah tampilan sudah login

      // Panggil Method getUserData yang anda implementasi dengan fungsi callback
      // yang menerima object user sebagai parameter.
      // Object user ini menerimarupakan object hasil response dari pemanggilan API Facebook.
      getUserData(user => {
        // Render tampilan profil, form postInput post, tombol post status, dan tombol logout
        $('#lab8').html(
          '<div class="profile">' +
            '<div class="img">' +
                '<img class="cover" src="' + user.cover.source + '" alt="cover" />' +
                '<img class="picture" src="' + user.picture.data.url + '" alt="profpic" />' +
            '</div>' +
            '<div class="data">' +
              '<h1>' + user.name + '</h1>' +
              '<h2>' + user.about + '</h2>' +
              '<h3>' + user.email + ' - ' + user.gender + '</h3>' +
            '</div>' +
          '</div>' +
          '<input id="postInput" type="text" class="post" placeholder="Write your status here" />' +
          '<button id="postStatus" onclick="postStatus()">Post to Facebook</button>' 
        );

        // Setelah merender tampilan diatas, dapatkan data home feed dari akun yang login
        // dengan memanggil method getUserFeed yang kalian implementasi sendiri.
        // Method itu harus menerima parameter berupa fungsi callback, dimana fungsi callback
        // ini akan menerima parameter object feed yang merupakan response dari pemanggilan API Facebook
        getUserFeed(feed => {
          feed.data.map(value => {
            // Render feed, kustomisasi sesuai kebutuhan.
            console.log(value);
            if (value.message && value.story) {
              $('#lab8').append(
                '<div class="flex fadeInDown">' +
                  '<div class="feed">' +
                    '<h1>' + value.message + '</h1>' +
                    '<h2>' + value.story + '</h2>' +
                    '<button class="delete" onclick=deleteStatus("' + value.id + '")>Delete</button>' +
                  '</div>' +
                '</div>'
              );

            } else if (value.message) {
              $('#lab8').append(
                '<div class="flex fadeInDown">' +
                  '<div class="feed">' +
                    '<h1>' + value.message + '</h1>' +
                    '<button class="delete" onclick=deleteStatus("' + value.id + '")>Delete</button>' +
                  '</div>' +
                '</div>'  
              );
            } else if (value.story) {
              $('#lab8').append(
                '<div class="flex fadeInDown">' +
                  '<div class="feed">' +
                    '<h2>' + value.story + '</h2>' +
                    '<button class="delete" onclick=deleteStatus("' + value.id + '")>Delete</button>' + 
                  '</div>' +
                '</div>'
              );
            }
          });
        });
      });
    }else{
      $('#lab8').html(
        '<div>' +
          '<img class="image" src="https://i.ytimg.com/vi/V00mAPdNIJg/maxresdefault.jpg">' +
        '</div>' +
        '<button class="Login" onclick="facebookLogin()">LOGIN NOW WITH FACEBOOK</button>'  
        );      
    } 
  };

  const facebookLogin = () => {
    // TODO: Implement Method Ini
    // Pastikan method memiliki callback yang akan memanggil fungsi render tampilan sudah login
    // ketika login sukses, serta juga fungsi ini memiliki segala permission yang dibutuhkan
    // pada scope yang ada.
    FB.login(function(response){
       console.log(response);
       location.reload();
     }, {scope:'public_profile,user_posts,publish_actions,email,user_about_me'})
  };

  const facebookLogout = () => {
    // TODO: Implement Method Ini
    // Pastikan method memiliki callback yang akan memanggil fungsi render tampilan belum login
    // ketika logout sukses.
     FB.getLoginStatus(function(response) {

        if (response.status === 'connected') {
            FB.logout();
            render(false);
            window.location.reload();

        }
    });

  };

  const getUserData = (render) => {
    // TODO: Implement Method Ini
    // Pastikan method ini menerima parameter berupa fungsi callback, lalu merequest data User dari akun
    // yang sedang login dengan semua fields yang dibutuhkan di method render, dan memanggil fungsi callback
    // tersebut setelah selesai melakukan request dan meneruskan response yang didapat ke fungsi callback
    // tersebut
    FB.getLoginStatus((response) => {
      if (response.status === 'connected') {
        FB.api('/me?fields=id,name,cover,picture.width(168).height(168),about,email,gender', 'GET', (response) => {
          render(response);
        });
      }
    });

  };

  const getUserFeed = (fun) => {
    // TODO: Implement Method Ini
    // Pastikan method ini menerima parameter berupa fungsi callback, lalu merequest data Home Feed dari akun
    // yang sedang login dengan semua fields yang dibutuhkan di method render, dan memanggil fungsi callback
    // tersebut setelah selesai melakukan request dan meneruskan response yang didapat ke fungsi callback
    // tersebut
    FB.getLoginStatus(function(response) {
        console.log('getUserFeed');
        if (response.status == 'connected') {
            FB.api('/me/feed', 'GET', function(response) {
                console.log(response);
                if (response && !response.error) {
                    /* handle the result */
                    console.log('halo');
                    fun(response);

                }
                else {
                    console.log('elo')
                }

            });
        }
    });

  };

  const postFeed = (message) => {
    // Todo: Implement method ini,
    // Pastikan method ini menerima parameter berupa string message dan melakukan Request POST ke Feed
    // Melalui API Facebook dengan message yang diterima dari parameter.
    FB.api('/me/feed', 'POST', {message:message});
    
    

  };

  const postStatus = () => {
    const message = $('#postInput').val();
    postFeed(message);
    render(true);
  };

  const deleteStatus = (id) => {

    FB.api('/'+ id, 'delete', function(response) {
    if (!response || response.error) {
      alert('Error occured');
    } else {
      alert('Post was deleted');
      render(true);
  }
});
  }